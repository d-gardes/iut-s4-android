package com.d_gardes.p222;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.d_gardes.p222.BDD.BDDJeu;
import com.d_gardes.p222.BDD.BDDJoueurs;
import com.d_gardes.p222.ModelePartie.ModelePartie;

/**
 * Activité qui pose les défis aux joueurs
 */

public class ChallengeActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_challenge);

		ModelePartie modele = (ModelePartie) this.getIntent().getSerializableExtra("MODELE");

		BDDJeu db = new BDDJeu(this);

		boolean isAction = this.getIntent().getBooleanExtra("ACTION", false);

		TextView title = findViewById(R.id.title);
		if(isAction) title.setText(R.string.truth);
		else title.setText(R.string.dare);

		TextView name = findViewById(R.id.name);
		name.setText(modele.nomJoueurCourant());

		TextView challenge = findViewById(R.id.challenge);
		challenge.setText(db.getChallenge(isAction));

		Button validated = findViewById(R.id.validated);
		View.OnClickListener validated_listner = (view) -> challengeFinished(modele, true);
		validated.setOnClickListener(validated_listner);

		Button failed = findViewById(R.id.failed);
		View.OnClickListener failed_listner = (view) -> challengeFinished(modele, false);
		failed.setOnClickListener(failed_listner);
	}

	/**
	 * Méthode qui gère la mise à jour du modèle et le bon aiguillage à la fin de chaque question
	 */
	private void challengeFinished(ModelePartie modele, boolean isValidated) {

		modele.tourSuivant(isValidated);
		if(modele.isPartieFinie()) {
			BDDJoueurs db = new BDDJoueurs(this);
			db.updateScoreJoueur(modele.nomGagnant());
			Intent intent = new Intent(this, WinnerActivity.class);
			intent.putExtra("WINNER", modele.nomGagnant());
			startActivity(intent);
		} else {
			Intent intent = new Intent(this, ActionOuVeriteActivity.class);
			intent.putExtra("MODELE", modele);
			startActivity(intent);
		}
		finish();
	}
}
