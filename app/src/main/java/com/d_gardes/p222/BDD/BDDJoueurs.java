package com.d_gardes.p222.BDD;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.d_gardes.p222.utils.Pair;

import java.util.ArrayList;


/**
 * Table qui gère le stockage des joueurs
 */

public class BDDJoueurs {

    private SQLiteDatabase db;

    public BDDJoueurs(Context context) {
        this.db = BDD.getInstance(context).getDataBase();
    }

    public void insertJoueurs(String prenom) {

        ContentValues valeurs = new ContentValues();
        valeurs.put("Prenom", prenom);
        valeurs.put("NbAction", 0);
        //Inserer valeur dans la BDD
        db.insert("Joueurs", null, valeurs);

    }

    public ArrayList<String> getJoueursNoms() { // Renvoie la liste des noms des joueurs

        ArrayList<String> arrayList = new ArrayList<>();
        Cursor curseur = db.rawQuery("SELECT * FROM Joueurs;", null);
        curseur.moveToFirst();
        while (!curseur.isAfterLast()) {
            arrayList.add(curseur.getString(curseur.getColumnIndex("Prenom")));
            curseur.moveToNext();
        }
        curseur.close();
        return arrayList;
    }

    public ArrayList<Pair<String, Integer>> getMeilleursJoueur() { // Renvoie la liste des noms des joueurs associés à leurs scores respectifs dans l'ordre du meilleur au plus faible
        ArrayList<Pair<String, Integer>> liste = new ArrayList<>();
	    Cursor res = db.rawQuery("SELECT * FROM Joueurs ORDER BY nbAction DESC LIMIT 11", null);
        res.moveToFirst();
        while(!res.isAfterLast()) {
            liste.add(new Pair<>(res.getString(res.getColumnIndex("Prenom")), res.getInt(res.getColumnIndex("NbAction"))));
            res.moveToNext();
        }
        res.close();
        return liste;
    }

	public void updateScoreJoueur(String nom) {
		Cursor req = db.rawQuery("SELECT NbAction FROM Joueurs WHERE prenom = \'" + nom + "\';", null);
		req.moveToFirst();
		int score = req.getInt(req.getColumnIndex("NbAction"));
		req.close();
		db.execSQL("UPDATE Joueurs SET NbAction = " + (score + 1) + " WHERE prenom = \'" + nom + "\';");
	}

    public void viderTableJoueurs() {
        db.execSQL("DELETE FROM Joueurs WHERE 1=1;");
    }
}
