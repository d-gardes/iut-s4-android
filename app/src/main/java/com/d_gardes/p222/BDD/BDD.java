package com.d_gardes.p222.BDD;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

//
// Created by  on 11/03/2020.
// Project P222
//

/**
 * Classe gérant la BDD
 *
 * Suit le design pattern Singleton à cause de SQLiteOpenHelper
 * Les classes BDDJoueur et BDDJeu utilisent celle ci pour avoir un accès à la BDD avec getDatabase()
 *
 * */

public class BDD extends SQLiteOpenHelper {

	private static BDD instance;
	private SQLiteDatabase db;


	static synchronized BDD getInstance(Context context){ // Pas de "public" pour n'être accessible que depuis le paquetage BDD
		if(instance == null){
			instance = new BDD(context);
		}
		return instance;
	}

	private BDD(Context context) {
		super(context, "BDD", null, 1);
		if (db == null) db = getWritableDatabase();
		instance = this;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		db.execSQL("CREATE TABLE IF NOT EXISTS Joueurs " +
				"(Prenom TEXT primary key , " +
				"NbAction integer not null);");

		db.execSQL("CREATE TABLE IF NOT EXISTS Jeu " +
				"(Intitule TEXT primary key , " +
				"Type INTEGER not null );");

		this.db = db;

		// ACTION
		insertJeu("Allez voir quelqu’un et faites-lui peur.", true);
		insertJeu("Dansez avec un balai ou une serpillière.", true);
		insertJeu("Chantez l’alphabet sans bouger la bouche.", true);
		insertJeu("Faites votre meilleure immitation de président.", true);
		insertJeu("Crie le premier mot qui te vient à l’esprit en ce moment.", true);
		insertJeu("Chantez tout ce que vous dites pendant les 10 prochaines minutes.", true);
		insertJeu("Dites l’alphabet à l’envers vous avez 15 secondes.", true);
		insertJeu("Laissez quelqu’un écrire un mot sur votre front au marqueur permanent.", true);
		insertJeu("Jusqu’au prochain round, parlez très fort, comme si personne ne pouvait vous entendre.", true);
		insertJeu("Tournez une dizaine de fois, quand vous avez fini, essayez de marcher en ligne droite.", true);

		// VERITE
		insertJeu("Quelle est ta pire honte ?", false);
		insertJeu("Quelle est ta pire bêtise étant enfant ?", false);
		insertJeu("Quel est ton rêve le plus bizarre", false);
		insertJeu("Il te reste 24h à vivre, qu’est-ce que tu fais ?", false);
		insertJeu("Quel est le pire cadeau qu’on t’a offert ?", false);
		insertJeu("Tu as une technique de drague secrète ?", false);
		insertJeu("Quel le pire mensonge que tu as raconté ?", false);
		insertJeu("Tu as un complexe ?", false);
		insertJeu("Est-ce que tu ronfles ?", false);
		insertJeu("Quelle est ta plus grande peur ?", false);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

	SQLiteDatabase getDataBase() {
		if (db == null)	db = getWritableDatabase();
		return db;
	}

	private void insertJeu(String intitule, boolean type) {
		// True = Action, False = Vérité

		ContentValues valeurs = new ContentValues();
		valeurs.put("Intitule", intitule);
		valeurs.put("Type", type);
		db.insert("Jeu", null, valeurs);
	}
}
