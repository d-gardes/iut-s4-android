package com.d_gardes.p222.BDD;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.Random;


/**
 * Table qui stocke les questions posées aux joueurs
 */


public class BDDJeu {

    private SQLiteDatabase db;

    public BDDJeu(Context context) { this.db = BDD.getInstance(context).getDataBase(); }

    public String getChallenge(boolean type) { // Renvoie une qestion
        Random rand = new Random();
        rand.setSeed(System.currentTimeMillis());
        int gageNo = rand.nextInt(10);

        Cursor curseur = db.rawQuery("SELECT intitule FROM Jeu WHERE type = " + (type ? 1 : 0) + ";", null);
        curseur.moveToFirst();
        for (int i = 0; i < gageNo; i++) {
            curseur.moveToNext();
        }
        String gage = curseur.getString(curseur.getColumnIndex("Intitule"));
        curseur.close();
        return gage;
    }

}