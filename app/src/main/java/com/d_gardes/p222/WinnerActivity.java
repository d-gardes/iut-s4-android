package com.d_gardes.p222;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Affiche le joueur gagnant et retourne automatiquement à l'acceuil
 */

public class WinnerActivity extends AppCompatActivity {

	Handler handler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_winner);

		TextView win = findViewById(R.id.winner);
		win.setText(win.getText().toString().replace("{$NOM}", this.getIntent().getStringExtra("WINNER")));

		handler = new Handler();
		handler.postDelayed(() -> {
			Intent intent = new Intent(this, MainActivity.class);
			startActivity(intent);
			finish();
		}, 3000);

	}

}
