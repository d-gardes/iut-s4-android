package com.d_gardes.p222.utils;
//
// Created by  on 10/03/2020.
// Project P222
//

import java.io.Serializable;

/**
 * Stocke une paire d'objets
 * @param <T>
 * @param <U>
 */

public class Pair <T, U> implements Serializable {

	private T premiere_valeur;
	private U seconde_valeur;

	public Pair(T premiere_valeur, U seconde_valeur) {
		this.premiere_valeur = premiere_valeur;
		this.seconde_valeur = seconde_valeur;
	}

	public T getPremiere_valeur() {
		return premiere_valeur;
	}

	public void setPremiere_valeur(T premiere_valeur) {
		this.premiere_valeur = premiere_valeur;
	}

	public U getSeconde_valeur() {
		return seconde_valeur;
	}

	public void setSeconde_valeur(U seconde_valeur) {
		this.seconde_valeur = seconde_valeur;
	}
}
