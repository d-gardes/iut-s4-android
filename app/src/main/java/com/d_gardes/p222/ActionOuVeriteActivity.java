package com.d_gardes.p222;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.d_gardes.p222.ModelePartie.ModelePartie;

/**
 * Choix du joueur entre un challenge d'action ou une véritée
 */

public class ActionOuVeriteActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_action_ou_verite);
		setTitle(R.string.title);

		ModelePartie modele = (ModelePartie) this.getIntent().getSerializableExtra("MODELE");

		TextView name = findViewById(R.id.name);
		name.setText(modele.nomJoueurCourant());

		findViewById(R.id.submit).setOnClickListener((view) -> {
			Intent intent = new Intent(this, ChallengeActivity.class);
			intent.putExtra("MODELE", modele);
			RadioButton truth = findViewById(R.id.truth);
			intent.putExtra("ACTION", truth.isChecked());
			startActivity(intent);
			finish();
		});
	}
}
