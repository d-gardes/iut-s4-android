package com.d_gardes.p222.ModelePartie;
//
// Created by  on 11/03/2020.
// Project P222
//

/**
 * Modèle qui gère la partie d'action ou vérité
 * Implémente Serializable pour pouvoir passer dans les Intent d'activité en activité
 */

import com.d_gardes.p222.utils.Pair;

import java.io.Serializable;
import java.util.ArrayList;

public class ModelePartie implements Serializable {

	private static final int NBR_TOUR_PAR_JOUEUR = 3; // Pour éviter des parties de 45min
	// NOTE: Pourrais être modifiable par l'utilisateur

	private int nbrTourEnCours;
	private ArrayList<Pair<String, Integer>> joueurs;

	public ModelePartie(ArrayList<String> listeNoms) {
		this.nbrTourEnCours = 0;
		this.joueurs = new ArrayList<>();
		for (String nom : listeNoms ) {
			this.joueurs.add(new Pair<>(nom, 0));
		}
	}

	public boolean isPartieFinie() {
		return nbrTourEnCours > this.joueurs.size() * NBR_TOUR_PAR_JOUEUR;
	}

	/**
	 * On ne gère pas les égalitées donc le premier dans l'ordre alphabétique gagne
	 */
	public String nomGagnant() throws IllegalStateException {
		if (this.isPartieFinie()) {
			Pair<String, Integer> gagnant = new Pair<>("", 0);
			for( Pair<String, Integer> joueur : this.joueurs ) {
				if (joueur.getSeconde_valeur() >= gagnant.getSeconde_valeur()) { gagnant = joueur; }
			}
			return gagnant.getPremiere_valeur();
		} else throw new IllegalStateException("Partie non finie !");
	}

	public String nomJoueurCourant() {
		return this.joueurs.get(this.nbrTourEnCours % this.joueurs.size()).getPremiere_valeur();
	}

	/**
	 * Passe au tour suivant
	 * Ne pas oublier l'appel à cette méthode sous peine d'entrer dans une partie infinie !
 	 */

	public void tourSuivant(boolean isDefiValide) {
		Pair<String, Integer> currentJoueur = this.joueurs.get(this.nbrTourEnCours % this.joueurs.size());
		if (isDefiValide) currentJoueur.setSeconde_valeur(currentJoueur.getSeconde_valeur() + 1);
		this.nbrTourEnCours ++;
	}

}
