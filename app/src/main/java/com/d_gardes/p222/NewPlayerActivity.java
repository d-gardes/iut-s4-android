package com.d_gardes.p222;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Renvoie le nom du nouveau joueur dans l'intent de retour.
 */

public class NewPlayerActivity extends AppCompatActivity {

	public static final String NAME_IDENTIFIER = "NOM_JOUEUR";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_player);

		Button submit = findViewById(R.id.submit);

		View.OnClickListener submit_listener = (View) -> {
			EditText name = findViewById(R.id.name);
			Intent retour = new Intent();
			retour.putExtra(NAME_IDENTIFIER, name.getText().toString());
			setResult(50, retour);
			finish();
		};
		submit.setOnClickListener(submit_listener);
	}
}
