package com.d_gardes.p222;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.d_gardes.p222.BDD.BDDJoueurs;
import com.d_gardes.p222.ModelePartie.ModelePartie;
import com.d_gardes.p222.listview.ListeJoueursAdapter;

/**
 * Activité qui permet de choisir les joueurs qui participent à la partie
 */

public class ListeJoueursActivity extends AppCompatActivity {

    private BDDJoueurs db = new BDDJoueurs(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_joueurs);
        setTitle(R.string.listeJoueurs);

        Button btnJeu = findViewById(R.id.submit);
        btnJeu.setOnClickListener((view) -> {
            ListView listeViewJoueurs = findViewById(R.id.listeJoueurs);
            ListeJoueursAdapter adapter = (ListeJoueursAdapter) listeViewJoueurs.getAdapter();
            if (adapter.getParticipants().size() > 1) {
                ModelePartie modele = new ModelePartie(adapter.getParticipants());
                Intent intent = new Intent(this, ActionOuVeriteActivity.class);
                intent.putExtra("MODELE", modele);
                startActivity(intent);
                finish();
            } else Toast.makeText(this, R.string.twoPlayersMin, Toast.LENGTH_SHORT).show();
        });

    }

    /**
     * On garde à jour la liste des joueurs de la BDD
     */
    @Override
    protected void onResume() {
        super.onResume();
        ListView listeViewJoueurs = findViewById(R.id.listeJoueurs);
        BDDJoueurs db = new BDDJoueurs(this);
        listeViewJoueurs.setAdapter(new ListeJoueursAdapter(this, db.getJoueursNoms()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_joueurs, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent nouveauJoueur = new Intent(this, NewPlayerActivity.class);
        startActivityForResult(nouveauJoueur, 50);
        return true;
    }

    /**
     * Gere les retours de NewPlayerActivity
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 50) {
            db.insertJoueurs(data.getStringExtra(NewPlayerActivity.NAME_IDENTIFIER));
            startActivity(getIntent());
            finish(); // Rechargement complet de l'activité pour éviter un problème
            // de chargement de données dans l'adapter de la listView.
            // C'est invisible pour l'utilisateur mais évite de mettre en place un RecyclerView bien trop lourd ici.
        }
    }
}
