package com.d_gardes.p222;

import android.os.Bundle;
import android.view.HapticFeedbackConstants;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.d_gardes.p222.BDD.BDDJoueurs;
import com.d_gardes.p222.listview.MeilleursScoresAdapter;

/**
 * Activité qui affiche les scores de tous les joueurs de la BDD triés par scores
 */

public class MeilleursScoresActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_meilleurs_scores);
		setTitle(R.string.highScores);

		ListView meilleursScores = findViewById(R.id.listeScores);
		meilleursScores.setAdapter(new MeilleursScoresAdapter(this));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.menu_meilleurs_scores, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(@NonNull MenuItem item) {

		if(item.getItemId() == R.id.menu_reset_high_scores) {
			BDDJoueurs db = new BDDJoueurs(this);
			db.viderTableJoueurs();
			ListView meilleursScores = findViewById(R.id.listeScores);
			meilleursScores.setAdapter(new MeilleursScoresAdapter(this));
			Toast.makeText(this, R.string.clearComplete, Toast.LENGTH_SHORT).show();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
