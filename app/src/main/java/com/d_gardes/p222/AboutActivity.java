package com.d_gardes.p222;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Affiche simplement la page "A propos"
 */

public class AboutActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		setTitle(R.string.about);
	}
}
