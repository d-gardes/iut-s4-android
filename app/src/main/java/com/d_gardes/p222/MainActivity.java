package com.d_gardes.p222;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.d_gardes.p222.BDD.BDDJoueurs;
import com.d_gardes.p222.utils.Pair;

import java.util.ArrayList;

/**
 * Activité principale où l'on atterit en lançant l'application
 */

public class MainActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setTitle(R.string.title);

		Button btn_about = findViewById(R.id.play);
		Button.OnClickListener btn_about_listener = (view) -> startActivity(new Intent(this, ListeJoueursActivity.class));
		btn_about.setOnClickListener(btn_about_listener);
	}

	/**
	 * Garde à jour le texte du meilleur joueur
	 */
	@Override
	protected void onResume() {
		super.onResume();
		BDDJoueurs db = new BDDJoueurs(this);
		ArrayList<Pair<String, Integer>> liste = db.getMeilleursJoueur();
		TextView msg = findViewById(R.id.textRecords);
		if (liste.size() == 0) msg.setText("");
		else if (liste.get(0).getSeconde_valeur() == 0) msg.setText("");
		else msg.setText(msg.getText().toString().replace("{$NAME}", liste.get(0).getPremiere_valeur()).replace("{$NBR}", liste.get(0).getSeconde_valeur().toString()));

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(@NonNull MenuItem item) {

		switch(item.getItemId()) {
			case R.id.menu_high_scores:
				startActivity(new Intent(this, MeilleursScoresActivity.class));
				return true;

			case R.id.menu_about:
				startActivity(new Intent(this, AboutActivity.class));
				return true;

			default:
				return super.onOptionsItemSelected(item);

		}
	}
}
