package com.d_gardes.p222.listview;
//
// Created by  on 10/03/2020.
// Project P222
//

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.d_gardes.p222.BDD.BDDJoueurs;
import com.d_gardes.p222.R;
import com.d_gardes.p222.utils.Pair;

import java.util.ArrayList;

/**
 * Adapter de la listView de "MeilleursScoresActivity"
 * qui liste les meilleurs joueurs
 */

public class MeilleursScoresAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private ArrayList<Pair<String, Integer>> meilleursJoueurs;

	public MeilleursScoresAdapter(Context context) {
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		BDDJoueurs db = new BDDJoueurs(context);
		this.meilleursJoueurs = db.getMeilleursJoueur();
	}

	@Override
	public int getCount() { return this.meilleursJoueurs.size(); }

	@Override
	public Pair<String, Integer> getItem(int position) {
		return this.meilleursJoueurs.get(position);
	}

	@Override
	public long getItemId(int position) { return position; }

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View vi = convertView;
		if (vi == null) vi = inflater.inflate(R.layout.row_score, null);

		// On pose le nom  du joueur ...
		TextView name = vi.findViewById(R.id.nomJoueur);
		name.setText(this.meilleursJoueurs.get(position).getPremiere_valeur());

		// ...et son score
		TextView scoreText = vi.findViewById(R.id.scoreJoueur);
		String score = this.meilleursJoueurs.get(position).getSeconde_valeur().toString();
		scoreText.setText(scoreText.getText().toString().replace("{$NBR}", score));

		return vi;
	}
}
