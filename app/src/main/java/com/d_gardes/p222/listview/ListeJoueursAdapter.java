package com.d_gardes.p222.listview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Switch;
import android.widget.TextView;

import com.d_gardes.p222.R;
import com.d_gardes.p222.utils.Pair;

import java.util.ArrayList;

//
// Created by  on 09/03/2020.
// Project P222
//

/**
 * Adapter de la listView de "ListeJoueursActivity"
 * qui liste les joueurs pour sélectionner les participants
 */

public class ListeJoueursAdapter extends BaseAdapter {

	private ArrayList<Pair<String, Switch>> joueurs;
	private LayoutInflater inflater;

	public ListeJoueursAdapter (Context context, ArrayList<String> listeJoueurs) {
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.joueurs = new ArrayList<>();
		for( String nom : listeJoueurs ) {
			this.joueurs.add(new Pair<>(nom, null));
		}
	}

	@Override
	public int getCount() { return joueurs.size(); }

	@Override
	public Pair<String, Switch> getItem(int position) { return joueurs.get(position); }

	@Override
	public long getItemId(int position) { return position;	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View vi = convertView;
		if (vi == null) vi = inflater.inflate(R.layout.row_joueur, null);

		Pair<String, Switch> currentPlayer = joueurs.get(position);

		// On pose le nom du joueur ...
		TextView name = vi.findViewById(R.id.nomJoueur);
		name.setText(currentPlayer.getPremiere_valeur());

		// ... et on récupère l'instance du Switch de la ligne pour pouvoir lister les participants
		currentPlayer.setSeconde_valeur(vi.findViewById(R.id.joue));

		return vi;
	}

	// On parcours les Switchs pour lister les participants à la partie
	public ArrayList<String> getParticipants() {
		ArrayList<String> participants = new ArrayList<>();
		for(Pair<String, Switch> joueur : this.joueurs ) {
			if (joueur.getSeconde_valeur().isChecked()) participants.add(joueur.getPremiere_valeur());
		}

		return participants;
	}
}
